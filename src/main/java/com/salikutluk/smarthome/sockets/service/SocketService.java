package com.salikutluk.smarthome.sockets.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.salikutluk.smarthome.sockets.command.CommandLineExecutor;
import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.repository.SocketRepository;

@Service
public class SocketService {

  private final CommandLineExecutor executor;
  private final SocketRepository repo;
  @Value("${send.script.path}")
  private String pathToScript;

  public SocketService(final CommandLineExecutor executor, final SocketRepository repo) {
    this.executor = executor;
    this.repo = repo;
  }

  public void sendRequestAndSave(final Socket socket) {
    executor.execute("python3", pathToScript, "-p", socket.getPulseLength(), "-t", socket.getProtocol(), socket.getCode());
    repo.save(socket);
  }

  public Socket getSocket(final long id) {
    return repo.getSocket(id);
  }


  public Socket save(final Socket socket) {
    return repo.save(socket);
  }
}
