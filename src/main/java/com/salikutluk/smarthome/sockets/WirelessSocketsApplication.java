package com.salikutluk.smarthome.sockets;

import static org.springframework.boot.SpringApplication.run;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.service.SocketService;

@SpringBootApplication
public class WirelessSocketsApplication {

  public WirelessSocketsApplication(final SocketService socketService) {
    socketService.save(
      new Socket(1L, "315", "1", "5755907", false)
    );
  }

  public static void main(final String... args) {
    run(WirelessSocketsApplication.class, args);
  }
}
