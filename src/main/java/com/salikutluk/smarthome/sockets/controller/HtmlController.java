package com.salikutluk.smarthome.sockets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.service.SocketService;

@Controller
@RequestMapping("/sockets")
public class HtmlController {

  private final SocketService socketService;

  public HtmlController(final SocketService socketService) {
    this.socketService = socketService;
  }

  @GetMapping
  public String getHtmlTemplateName(final Model model) {
    model.addAttribute("socket_1", socketService.getSocket(1L));
    return "socketPage";
  }

  @PostMapping
  public String toggleSocket(final Model model) {
    final Socket socket = socketService.getSocket(1L);
    final boolean socketIsOn = socket.isOn();
    if (socketIsOn) {
      socket.setCode("5755907");
    } else {
      socket.setCode("5755916");
    }
    socket.setOn(!socketIsOn);
    socketService.sendRequestAndSave(socket);
    model.addAttribute("socket_1", socketService.getSocket(1L));
    return "socketPage";
  }
}
