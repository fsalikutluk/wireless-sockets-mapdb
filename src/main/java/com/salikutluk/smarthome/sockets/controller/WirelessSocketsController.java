package com.salikutluk.smarthome.sockets.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.salikutluk.smarthome.sockets.model.Socket;
import com.salikutluk.smarthome.sockets.model.response.StatusResponse;
import com.salikutluk.smarthome.sockets.service.SocketService;

@RestController
public class WirelessSocketsController {

  private final SocketService socketService;

  public WirelessSocketsController(final SocketService socketService) {
    this.socketService = socketService;
  }

  @GetMapping("/status/{id}")
  public ResponseEntity<StatusResponse> getResponse(@PathVariable("id") final int id) {
    return ResponseEntity.ok(new StatusResponse(socketService.getSocket(id).isOn()));
  }

  @PostMapping("/turnOn")
  public ResponseEntity<HttpStatus> turnOn(@RequestBody final Socket socket) {
    return sendRequest(socket);
  }

  @PostMapping("/turnOff")
  public ResponseEntity<HttpStatus> turnOff(@RequestBody final Socket socket) {
    return sendRequest(socket);
  }

  private ResponseEntity<HttpStatus> sendRequest(final Socket socket) {
    socketService.sendRequestAndSave(socket);
    return ResponseEntity.ok().build();
  }
}
