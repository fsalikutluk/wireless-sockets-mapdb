package com.salikutluk.smarthome.sockets.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.salikutluk.smarthome.sockets.model.Socket;

@Service
public class SocketRepository {

  private final Map<Long, Socket> memoryDB = new ConcurrentHashMap<>();

  public Socket save(final Socket socket) {
    final long id = socket.getId();
    memoryDB.put(id, socket);
    return getSocket(id);
  }

  public Socket getSocket(final long id) {
    return memoryDB.get(id);
  }

  public int count() {
    return memoryDB.size();
  }
}
