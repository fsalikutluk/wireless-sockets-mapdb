package com.salikutluk.smarthome.sockets.command;

import org.springframework.stereotype.Component;

import com.salikutluk.smarthome.commandline.CommandLineExecuter;

@Component
public class CommandLineExecutor {

  public String execute(final String... args) {
    final CommandLineExecuter commandLineExecutor = new CommandLineExecuter();
    return commandLineExecutor.executeCommand(args);
  }
}
