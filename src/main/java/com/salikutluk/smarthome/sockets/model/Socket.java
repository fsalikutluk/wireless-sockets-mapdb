package com.salikutluk.smarthome.sockets.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Socket {

  private long id;
  private String pulseLength;
  private String protocol;
  private String code;
  private boolean isOn;
}
