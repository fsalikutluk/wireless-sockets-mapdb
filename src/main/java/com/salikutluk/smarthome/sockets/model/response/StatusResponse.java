package com.salikutluk.smarthome.sockets.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class StatusResponse {

  private boolean statusPattern;
}
