package com.salikutluk.smarthome.sockets.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CommandExecutorTest {

  private static final String TEST_STRING = "HelloWorld";

  @Test
  public void testCommandExecutorSynch() {
    final CommandLineExecutor commandLineExecutor = new CommandLineExecutor();
    final String response = commandLineExecutor.execute("echo", TEST_STRING);
    final String response1 = commandLineExecutor.execute("echo", TEST_STRING + 123);
    final String response2 = commandLineExecutor.execute("echo", TEST_STRING + 456);
    assertEquals(TEST_STRING, response);
    assertEquals(TEST_STRING + 123, response1);
    assertEquals(TEST_STRING + 456, response2);
  }

  @Test
  public void testCommandExecutorAsync() {
    final CommandLineExecutor executor = new CommandLineExecutor();
    final String response = executor.execute("echo", TEST_STRING);
    new Thread(() -> {
      var response1 = executor.execute("echo", TEST_STRING + 123);
      assertEquals(TEST_STRING + 123, response1);
    }).start();
    new Thread(() -> {
      var response2 = executor.execute("echo", TEST_STRING + 456);
      assertEquals(TEST_STRING + 456, response2);
    }).start();
    assertEquals(TEST_STRING, response);
  }
}
