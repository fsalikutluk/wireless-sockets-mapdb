package com.salikutluk.smarthome.sockets.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.salikutluk.smarthome.sockets.model.Socket;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocketServiceTest {

  @Autowired
  private SocketService socketService;

  @Test
  public void testServiceExecutesAndSaves() {
    final Socket socket = new Socket(1, "pulseLength", "protocol", "code", true);
    socketService.sendRequestAndSave(socket);
    final Socket foundSocket = socketService.getSocket(1L);
    assertThat(foundSocket.isOn()).isTrue();
  }
}
