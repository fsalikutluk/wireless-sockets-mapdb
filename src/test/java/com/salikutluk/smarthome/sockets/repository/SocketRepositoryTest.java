package com.salikutluk.smarthome.sockets.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.salikutluk.smarthome.sockets.model.Socket;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocketRepositoryTest {

  @Autowired
  private SocketRepository socketRepository;

  @Before
  public void setup() {
    socketRepository.save(new Socket(1, "pulseLength", "protocol", "code", true));
  }

  @Test
  public void testFind() {
    final Socket socket = socketRepository.getSocket(1L);
    assertThat(socket.isOn()).isTrue();
  }

  @Test
  public void testUpdate() {
    final Socket socket = socketRepository.save(new Socket(1, "pulseLength", "protocol", "code", false));
    assertThat(socket.isOn()).isFalse();
    assertThat(socketRepository.count()).isEqualTo(1);
  }
}
